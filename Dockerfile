FROM python:2.7.15-alpine

ADD requirements.txt /tmp/requirements.txt
ADD requirements-dev.txt /tmp/requirements-dev.txt

# Dependências do Pillow:
# build-base jpeg-dev python-dev py-pip zlib-dev
# Dependências do psycopg2:
# gcc musl-dev postgresql-dev
RUN apk update && \
    apk add \
       build-base \
       gcc \
       jpeg-dev \
       musl-dev \
       postgresql-dev \
       py-pip \
       python-dev \
       tzdata  \
       zlib-dev && \
    cp /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime && \
    echo "America/Sao_Paulo" >  /etc/timezone && \
    pip --no-cache install -r /tmp/requirements-dev.txt && \
    apk del --purge build-base gcc musl-dev

ADD ./radarlegislativo /srv/radarlegislativo

# Precisamos das variáveis de ambiente no docker para rodar o
# collectstatic e o syncdb.
ADD .env /srv/radarlegislativo/.env

WORKDIR /srv/radarlegislativo

RUN python manage.py collectstatic --noinput

EXPOSE 8000
CMD ["gunicorn", "-b", "0.0.0.0:8000", "radarlegislativo.wsgi:application"]
