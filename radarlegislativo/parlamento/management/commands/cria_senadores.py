# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.core.management.base import BaseCommand
from tapioca_senado import Senado

from parlamento.models import Senador


class Command(BaseCommand):
    help = u"Popula banco com todos os senadores em exercício atualmente"

    def handle(self, *args, **options):
        senadores = []
        senado = Senado()
        resposta = senado.senadores().get()
        for senador in resposta.ListaParlamentarEmExercicio.Parlamentares.Parlamentar:
            data = senador.IdentificacaoParlamentar._data
            senadores.append(Senador(
                id_api=data['CodigoParlamentar'],
                nome=data['NomeCompletoParlamentar'],
                nome_parlamentar=data['NomeParlamentar'],
                foto=data['UrlFotoParlamentar'],
                pagina_oficial=data['UrlPaginaParlamentar'],
                email=data.get('EmailParlamentar'),
                partido_id=data['SiglaPartidoParlamentar'],
                estado_id=data['UfParlamentar'],
                participacao=data.get('DescricaoParticipacao'),
            ))

        Senador.objects.bulk_create(senadores)
