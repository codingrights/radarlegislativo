# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-04-05 19:18
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0019_auto_20171018_1100'),
    ]

    operations = [
        migrations.AddField(
            model_name='projeto',
            name='cadastro',
            field=models.DateField(auto_now_add=True, default=datetime.date(1970, 1, 1)),
            preserve_default=False,
        ),
    ]
