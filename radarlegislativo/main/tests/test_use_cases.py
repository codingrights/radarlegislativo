# coding: utf-8
import vcr
from datetime import date, datetime
from model_mommy import mommy
from mock import Mock, patch
from pytz import timezone
from unittest import TestCase

from django.test import tag

from main.models import Projeto, Tramitacao
from main.services import fetch_camara_project, fetch_senado_project
from main.use_cases import BaseFetchProjects, FetchProjectsFromCamara, FetchProjectsFromSenado

from suds.cache import NoCache


@patch('suds.cache.ObjectCache', Mock(NoCache))
class FetchProjectsFromCamaraTestCase(TestCase):

    def setUp(self):
        self.uc = FetchProjectsFromCamara()

    def tearDown(self):
        Projeto.objects.all().delete()

    def test_initialize_instance_correctly(self):
        assert isinstance(self.uc, BaseFetchProjects)
        assert fetch_camara_project == self.uc.get_service()
        assert Projeto.CAMARA == self.uc.get_source()

    @patch('main.use_cases.fetch_camara_project', spec=True)
    def test_execute_calls_service_correctly(self, m_service):
        m_service.return_value = {
            'apensadas': u'PL 6667/2009',
            'apresentacao': datetime(
                2004, 8, 11, 0, 0
            ).replace(tzinfo=timezone('America/Sao_Paulo')),
            'autoria': u'Cludio Magr\xe3o',
            'ementa': u'Dispõe sobre os limites à concentração econômica nos meios de comunicação social, e dá outras providências.',
            'html_original': '<html></htm>',
            'local': u'Comissão Xurumelas',
            'nome': u'PL 4026/2004',
            'tramitacoes': [{
                'data': datetime(2004, 8, 24).replace(tzinfo=timezone('America/Sao_Paulo')),
                'descricao': u'Encaminhada \xe0 publica\xe7\xe3o. Publica\xe7\xe3o Inicial no DCD 25 08 04 PAG 36404 COL 02. Inteiro teor',
                'local': u'COORDENA\xc7\xc3O DE COMISS\xd5ES PERMANENTES ( CCP )',
                'id_site': '4ba90b0b2a9fa8dd5b38ae2396522171',
            }]
        }

        assert 0 == Projeto.objects.count()
        assert 0 == Tramitacao.objects.count()

        self.uc.execute(1)

        m_service.assert_called_once_with(1)

    @tag('depends_on_order')  # order das tramitações difere usando Docker
    @vcr.use_cassette('main/tests/fixtures/vcr/project_camara_2150720.yaml')
    def test_execute_creates_instance_not_previously_created(self):
        camara_id = 2150720
        tags = mommy.make('main.Tag', _quantity=2)
        keywords = mommy.make('main.PalavraChave', _quantity=4)
        assert 0 == Projeto.objects.filter(id_site=camara_id).count()

        self.uc.execute(
            camara_id,
            tag_ids=[t.id for t in tags],
            keywords_ids=[k.id for k in keywords]
        )

        instance = Projeto.objects.get(id_site=camara_id)

        assert u'PEC 359/2017' == instance.nome
        assert u'Roberto Freire' == instance.autoria
        assert u'Acrescenta parágrafos ao artigo 218 da Constituição Federal, para garantir recursos mínimos à ciência, tecnologia e inovação.' == instance.ementa
        assert date(2017, 9, 6) == instance.apresentacao
        assert u'' == instance.apensadas
        assert u'Comissão Parecer Comissão de Constituição e Justiça e de Cidadania   ( CCJC ) -' == instance.local

        assert 2 == instance.tags.count()
        for t in tags:
            assert t in instance.tags.all()

        assert 4 == instance.palavras_chave.count()
        for k in keywords:
            assert k in instance.palavras_chave.all()

        assert 7 == instance.tramitacao_set.count()
        tramitacoes = instance.tramitacao_set.all()

        assert u'2efbda1aa07f9305717c0d6d8114eeb1' == tramitacoes[0].id_site
        assert date(2017, 12, 13) == tramitacoes[0].data
        assert u'Comissão de Constituição e Justiça e de Cidadania ( CCJC )' == tramitacoes[0].local

        assert u'6cec6fb96e113b51a51cdee41ad1b83a' == tramitacoes[1].id_site
        assert date(2017, 9, 20) == tramitacoes[1].data
        assert u'COORDENAÇÃO DE COMISSÕES PERMANENTES ( CCP )' == tramitacoes[1].local

        assert u'a43c04f53edfa1fa7df0d24434b7a79d' == tramitacoes[2].id_site
        assert date(2017, 9, 20) == tramitacoes[2].data
        assert u'Comissão de Constituição e Justiça e de Cidadania ( CCJC )' == tramitacoes[2].local

        assert u'2012b33fec10cac4193c09705ac1192d' == tramitacoes[3].id_site
        assert date(2017, 9, 15) == tramitacoes[3].data
        assert u'Mesa Diretora da Câmara dos Deputados ( MESA )' == tramitacoes[3].local

        assert u'e0a1bea450877f21761848f20d2950a2' == tramitacoes[4].id_site
        assert date(2017, 9, 11) == tramitacoes[4].data
        assert u'COORDENAÇÃO DE COMISSÕES PERMANENTES ( CCP )' == tramitacoes[4].local

        assert u'6834dda1d3951d888b2dc8ddb6e44f93' == tramitacoes[5].id_site
        assert date(2017, 9, 11) == tramitacoes[5].data
        assert u'Mesa Diretora da Câmara dos Deputados ( MESA )' == tramitacoes[5].local

        assert u'debb595585db3f400066698d9c845c46' == tramitacoes[6].id_site
        assert date(2017, 9, 6) == tramitacoes[6].data
        assert u'PLENÁRIO ( PLEN )' == tramitacoes[6].local

    @vcr.use_cassette('main/tests/fixtures/vcr/project_camara_2150720.yaml')
    def test_execute_updates_instance_previously_created(self):
        camara_id = 2150720
        tags = mommy.make('main.Tag', _quantity=2)
        mommy.make('main.Projeto', id_site=camara_id, origem=Projeto.CAMARA)

        assert 1 == Projeto.objects.filter(id_site=camara_id).count()

        self.uc.execute(camara_id, tag_ids=[t.id for t in tags], previously_created=True)

        instance = Projeto.objects.get(id_site=camara_id)

        assert u'PEC 359/2017' == instance.nome
        assert u'Roberto Freire' == instance.autoria
        assert u'Acrescenta parágrafos ao artigo 218 da Constituição Federal, para garantir recursos mínimos à ciência, tecnologia e inovação.' == instance.ementa
        assert date(2017, 9, 6) == instance.apresentacao
        assert u'' == instance.apensadas
        assert u'Comissão Parecer Comissão de Constituição e Justiça e de Cidadania   ( CCJC ) -' == instance.local

        assert 0 == instance.tags.count()
        assert 0 == instance.tramitacao_set.count()

    @tag('depends_on_order')  # order das tramitações difere usando Docker
    @vcr.use_cassette('main/tests/fixtures/vcr/project_camara_2150720.yaml')
    def test_execute_creates_instance_without_tags_correctly(self):
        camara_id = 2150720
        assert 0 == Projeto.objects.filter(id_site=camara_id).count()

        self.uc.execute(camara_id)

        instance = Projeto.objects.get(id_site=camara_id)

        assert u'PEC 359/2017' == instance.nome
        assert u'Roberto Freire' == instance.autoria
        assert u'Acrescenta parágrafos ao artigo 218 da Constituição Federal, para garantir recursos mínimos à ciência, tecnologia e inovação.' == instance.ementa
        assert date(2017, 9, 6) == instance.apresentacao
        assert u'' == instance.apensadas
        assert u'Comissão Parecer Comissão de Constituição e Justiça e de Cidadania   ( CCJC ) -' == instance.local

        assert 0 == instance.tags.count()

        assert 7 == instance.tramitacao_set.count()
        tramitacoes = instance.tramitacao_set.all()

        assert u'2efbda1aa07f9305717c0d6d8114eeb1' == tramitacoes[0].id_site
        assert date(2017, 12, 13) == tramitacoes[0].data
        assert u'Comissão de Constituição e Justiça e de Cidadania ( CCJC )' == tramitacoes[0].local

        assert u'6cec6fb96e113b51a51cdee41ad1b83a' == tramitacoes[1].id_site
        assert date(2017, 9, 20) == tramitacoes[1].data
        assert u'COORDENAÇÃO DE COMISSÕES PERMANENTES ( CCP )' == tramitacoes[1].local

        assert u'a43c04f53edfa1fa7df0d24434b7a79d' == tramitacoes[2].id_site
        assert date(2017, 9, 20) == tramitacoes[2].data
        assert u'Comissão de Constituição e Justiça e de Cidadania ( CCJC )' == tramitacoes[2].local

        assert u'2012b33fec10cac4193c09705ac1192d' == tramitacoes[3].id_site
        assert date(2017, 9, 15) == tramitacoes[3].data
        assert u'Mesa Diretora da Câmara dos Deputados ( MESA )' == tramitacoes[3].local

        assert u'e0a1bea450877f21761848f20d2950a2' == tramitacoes[4].id_site
        assert date(2017, 9, 11) == tramitacoes[4].data
        assert u'COORDENAÇÃO DE COMISSÕES PERMANENTES ( CCP )' == tramitacoes[4].local

        assert u'6834dda1d3951d888b2dc8ddb6e44f93' == tramitacoes[5].id_site
        assert date(2017, 9, 11) == tramitacoes[5].data
        assert u'Mesa Diretora da Câmara dos Deputados ( MESA )' == tramitacoes[5].local

        assert u'debb595585db3f400066698d9c845c46' == tramitacoes[6].id_site
        assert date(2017, 9, 6) == tramitacoes[6].data
        assert u'PLENÁRIO ( PLEN )' == tramitacoes[6].local


class FetchProjectsFromSenadoTestCase(TestCase):

    def setUp(self):
        self.uc = FetchProjectsFromSenado()

    def tearDown(self):
        Projeto.objects.all().delete()

    def test_initialize_instance_correctly(self):
        assert isinstance(self.uc, BaseFetchProjects)
        assert fetch_senado_project == self.uc.get_service()
        assert Projeto.SENADO == self.uc.get_source()

    @patch('main.use_cases.fetch_senado_project', spec=True)
    def test_execute_calls_service_correctly(self, m_service):
        m_service.return_value = {
            'apensadas': u'PL 6667/2009',
            'apresentacao': datetime(
                2004, 8, 11, 0, 0
            ).replace(tzinfo=timezone('America/Sao_Paulo')),
            'autoria': u'Cludio Magr\xe3o',
            'ementa': u'Dispõe sobre os limites à concentração econômica nos meios de comunicação social, e dá outras providências.',
            'html_original': '<html></htm>',
            'local': u'Comissão Xurumelas',
            'nome': u'PL 4026/2004',
            'tramitacoes': [{
                'data': datetime(2004, 8, 24).replace(tzinfo=timezone('America/Sao_Paulo')),
                'descricao': u'Encaminhada \xe0 publica\xe7\xe3o. Publica\xe7\xe3o Inicial no DCD 25 08 04 PAG 36404 COL 02. Inteiro teor',
                'local': u'COORDENA\xc7\xc3O DE COMISS\xd5ES PERMANENTES ( CCP )',
                'id_site': '4ba90b0b2a9fa8dd5b38ae2396522171',
            }]
        }

        assert 0 == Projeto.objects.count()
        assert 0 == Tramitacao.objects.count()

        self.uc.execute(1)

        m_service.assert_called_once_with(1)

    @tag('depends_on_order')  # order das tramitações difere usando Docker
    @vcr.use_cassette('main/tests/fixtures/vcr/project_senado_2150720.yaml')
    def test_execute_creates_instance_not_previously_created(self):
        senado_id = 113947
        tags = mommy.make('main.Tag', _quantity=2)
        assert 0 == Projeto.objects.filter(id_site=senado_id).count()

        self.uc.execute(senado_id, tag_ids=[t.id for t in tags])

        instance = Projeto.objects.get(id_site=senado_id)

        assert u'PLS 330/2013' == instance.nome
        assert u'SENADOR Antonio Carlos Valadares' == instance.autoria
        assert u'Dispõe sobre a proteção, o tratamento e o uso dos dados pessoais, e dá outras providências.' == instance.ementa
        assert date(2013, 8, 13) == instance.apresentacao
        assert u'PLS 181, PLS 131' == instance.apensadas
        assert u'Plenário do Senado Federal' == instance.local

        assert 2 == instance.tags.count()
        for t in tags:
            assert t in instance.tags.all()

        assert 122 == instance.tramitacao_set.count()
        tramitacoes = instance.tramitacao_set.all()

        assert u'2440888' == tramitacoes[0].id_site
        assert date(2018, 7, 10) == tramitacoes[0].data
        assert u'Coordenação de Arquivo' == tramitacoes[0].local

        assert u'2078760' == tramitacoes[121].id_site
        assert date(2013, 8, 13) == tramitacoes[121].data
        assert u'SUBSECRETARIA DE ATA - PLENÁRIO' == tramitacoes[121].local

    @vcr.use_cassette('main/tests/fixtures/vcr/project_senado_2150720.yaml')
    def test_execute_updates_instance_previously_created(self):
        senado_id = 113947
        tags = mommy.make('main.Tag', _quantity=2)
        mommy.make('main.Projeto', id_site=senado_id, origem=Projeto.SENADO)

        assert 1 == Projeto.objects.filter(id_site=senado_id).count()

        self.uc.execute(senado_id, tag_ids=[t.id for t in tags], previously_created=True)

        instance = Projeto.objects.get(id_site=senado_id)

        assert u'PLS 330/2013' == instance.nome
        assert u'SENADOR Antonio Carlos Valadares' == instance.autoria
        assert u'Dispõe sobre a proteção, o tratamento e o uso dos dados pessoais, e dá outras providências.' == instance.ementa
        assert date(2013, 8, 13) == instance.apresentacao
        assert u'PLS 181, PLS 131' == instance.apensadas
        assert u'Plenário do Senado Federal' == instance.local

        assert 0 == instance.tags.count()
        assert 0 == instance.tramitacao_set.count()

    @tag('depends_on_order')  # order das tramitações difere usando Docker
    @vcr.use_cassette('main/tests/fixtures/vcr/project_senado_2150720.yaml')
    def test_execute_creates_instance_without_tags_correctly(self):
        senado_id = 113947
        assert 0 == Projeto.objects.filter(id_site=senado_id).count()

        self.uc.execute(senado_id)

        instance = Projeto.objects.get(id_site=senado_id)

        assert u'PLS 330/2013' == instance.nome
        assert u'SENADOR Antonio Carlos Valadares' == instance.autoria
        assert u'Dispõe sobre a proteção, o tratamento e o uso dos dados pessoais, e dá outras providências.' == instance.ementa
        assert date(2013, 8, 13) == instance.apresentacao
        assert u'PLS 181, PLS 131' == instance.apensadas
        assert u'Plenário do Senado Federal' == instance.local

        assert 0 == instance.tags.count()

        assert 122 == instance.tramitacao_set.count()
        tramitacoes = instance.tramitacao_set.all()

        assert u'2440888' == tramitacoes[0].id_site
        assert date(2018, 7, 10) == tramitacoes[0].data
        assert u'Coordenação de Arquivo' == tramitacoes[0].local

        assert u'2078760' == tramitacoes[121].id_site
        assert date(2013, 8, 13) == tramitacoes[121].data
        assert u'SUBSECRETARIA DE ATA - PLENÁRIO' == tramitacoes[121].local
