# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from api.forms import FormularioProjeto
from main.use_cases import FetchProjectsFromCamara, FetchProjectsFromSenado


@csrf_exempt
@require_POST
def novo_projeto(request):
    form = FormularioProjeto(request.POST)
    if not form.is_valid():
        return JsonResponse(form.errors, status=400)

    switch = {'CA': FetchProjectsFromCamara(), 'SE': FetchProjectsFromSenado()}
    keywords, tags = form.cleaned_data['palavras_chave']

    download = switch.get(form.cleaned_data['origem'])
    download.execute(
        external_id=form.cleaned_data['id_site'],
        keywords_ids=tuple(keyword.id for keyword in keywords),
        tag_ids=tuple(tag.id for tag in tags),
        publish=False
    )
    return JsonResponse(_serialize(form), status=201)


def _serialize(form):
    data = {}
    for key in form._meta.fields:
        if key != "token":
            data[key] = form.cleaned_data[key]
    return data
