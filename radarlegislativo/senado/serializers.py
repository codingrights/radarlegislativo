import xmltodict


def serialize(xml_data):
    return xmltodict.parse(xml_data)
