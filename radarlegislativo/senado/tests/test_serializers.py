from mock import patch
from unittest import TestCase

from senado.clients import serializers


class SerializeTestCase(TestCase):

    @patch('senado.clients.serializers.xmltodict')
    def test_calls_lib_correctly(self, m_xmltodict):
        m_xmltodict.parse.return_value = 1

        data = serializers.serialize('xml')
        m_xmltodict.parse.assert_called_once_with('xml')

        assert 1 == data
