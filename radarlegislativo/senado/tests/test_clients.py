from mock import Mock, patch
from unittest import TestCase

from senado.clients import APISenadoClient, APISenadoMovimentacaoClient, BaseClient
from senado.exceptions import ClientConnectionError


class APISenadoClientTestCase(TestCase):

    def setUp(self):
        self.client = APISenadoClient()
        patchers = [
            patch('senado.clients.requests', spec=True),
            patch('senado.clients.serializers', spec=True),
        ]
        self.m_requests, self.m_serializer = [p.start() for p in patchers]
        self.m_requests.codes.OK = 200
        [self.addCleanup(p.stop) for p in patchers]

    def test_class_has_correct_setup(self):
        expected_url = u'http://legis.senado.leg.br/dadosabertos/materia'
        assert expected_url == self.client.get_absolute_url()
        assert isinstance(self.client, BaseClient)

    def test_get_returns_data_correctly(self):
        expected_url = u'http://legis.senado.leg.br/dadosabertos/materia/1'
        self.m_requests.get.return_value = Mock(
            status_code=200,
            content='content'
        )
        self.m_serializer.serialize.return_value = {'b': 2}

        data = self.client.get(1)

        self.m_requests.get.assert_called_with(expected_url)
        self.m_serializer.serialize.assert_called_once_with(self.m_requests.get().content)
        assert {'b': 2, 'content': 'content'} == data

    def test_get_raises_exception_when_response_contains_error(self):
        self.m_requests.get.return_value = Mock(status_code=500)

        self.assertRaises(ClientConnectionError, self.client.get, 1)


class APISenadoMovimentacaoClientTestCase(TestCase):

    def setUp(self):
        self.client = APISenadoMovimentacaoClient()
        patchers = [
            patch('senado.clients.requests', spec=True),
            patch('senado.clients.serializers', spec=True),
        ]
        self.m_requests, self.m_serializer = [p.start() for p in patchers]
        self.m_requests.codes.OK = 200
        [self.addCleanup(p.stop) for p in patchers]

    def test_class_has_correct_setup(self):
        expected_url = u'http://legis.senado.leg.br/dadosabertos/materia/movimentacoes'
        assert expected_url == self.client.get_absolute_url()
        assert isinstance(self.client, BaseClient)

    def test_get_returns_data_correctly(self):
        expected_url = u'http://legis.senado.leg.br/dadosabertos/materia/movimentacoes/1'
        self.m_requests.get.return_value = Mock(status_code=200)
        self.m_serializer.serialize.return_value = {'b': 2}

        data = self.client.get(1)

        self.m_requests.get.assert_called_with(expected_url)
        self.m_serializer.serialize.assert_called_once_with(self.m_requests.get().content)
        assert {'b': 2} == data

    def test_get_raises_exception_when_response_contains_error(self):
        self.m_requests.get.return_value = Mock(status_code=500)

        self.assertRaises(ClientConnectionError, self.client.get, 1)
