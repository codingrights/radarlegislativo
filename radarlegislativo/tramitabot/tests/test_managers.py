# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2018 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.test import TestCase
from model_mommy import mommy

from tramitabot.models import TramitabotUser


class TesteTramitabotUserManager(TestCase):

    def test_mark_user_as_blocked(self):
        mommy.make(
            'TramitabotUser',
            telegram_id=111111111,
            has_blocked_us=False,
        )
        TramitabotUser.objects.mark_as_blocked(111111111)
        self.assertTrue(TramitabotUser.objects.get(
            telegram_id=111111111).has_blocked_us)

    def test_mark_user_as_blocked_doesnt_affect_other_users(self):
        mommy.make(
            'TramitabotUser',
            telegram_id=111111111,
            has_blocked_us=False,
        )
        TramitabotUser.objects.mark_as_blocked(222222222)
        self.assertFalse(TramitabotUser.objects.get(
            telegram_id=111111111).has_blocked_us)

    def test_mark_user_as_blocked_accepts_inexistent_telegram_id(self):
        TramitabotUser.objects.mark_as_blocked(111111111)
