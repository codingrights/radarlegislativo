#  -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2017 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import datetime
from django import forms

from main.models import Projeto, Tag
from parlamento.models import ComissaoCamara, ComissaoSenado


ORIGEM_DEFAULT = "Todas"


def get_comissoes():
    return (
        ("", "Todas"),
        ("Senado", tuple(
            ComissaoSenado.objects.all().values_list('id', 'nome')),
        ),
        ("Câmara", tuple(
            ComissaoCamara.objects.all().values_list('id', 'nome')),
        ),
    )


def get_tags():
    return ((tag.id, tag.nome) for tag in Tag.objects.all())


class CustomCheckboxSelectMultiple(forms.CheckboxSelectMultiple):
    template_name = "website/checkbox_select_multiple.html"


class FilterForm(forms.Form):
    categoria = forms.TypedMultipleChoiceField(
        required=False,
        widget=CustomCheckboxSelectMultiple(
            attrs={"class": "submit-on-change"}
        ),
        choices=get_tags,
        coerce=int,
    )
    comissao = forms.TypedChoiceField(
        required=False,
        empty_value=None,
        choices=get_comissoes,
        widget=forms.Select(
            attrs={"class": "form-control submit-on-change"}
        ),
        coerce=int,
    )
    origem = forms.MultipleChoiceField(
        required=False,
        widget=CustomCheckboxSelectMultiple(
            attrs={"class": "submit-on-change"}
        ),
        choices=Projeto.ORIGEM_CHOICES,
    )
    impacto = forms.MultipleChoiceField(
        required=False,
        widget=CustomCheckboxSelectMultiple(
            attrs={"class": "submit-on-change"}
        ),
        choices=Projeto.IMPACTO_CHOICES,
    )

    q = forms.CharField(
        required=False,
        max_length=255,
        widget=forms.TextInput(
            attrs={
                "class": "form-control sumbit-on-enter",
                "placeholder": u"buscar",
            }
        ),
    )

    ordem = forms.ChoiceField(
        required=False,
        widget=forms.Select(
            attrs={"class": "form-control submit-on-change"}
        ),
        choices=(
            ("", "ordenar por últimas tramitações"),
            ("novos", "ordenar por mais recentes"),
            ("urgentes", "destacar urgentes"),
            ("promulgados", "destacar promulgados"),
            ("arquivados", "destacar arquivados"),
        )
    )


class FormListagemSemanal(forms.Form):
    origem_default = ORIGEM_DEFAULT
    data = forms.DateField(
        required=True,
        widget=forms.DateInput(
            attrs={
                "class": "form-control text-center submit-on-change",
                "id": "datepicker",
            },
            format="%d/%m/%Y",
        ),
        initial=datetime.date.today,
    )
    origem = forms.ChoiceField(
        required=False,
        widget=forms.Select(
            attrs={"class": "form-control text-center submit-on-change"}
        ),
        choices=(("", ORIGEM_DEFAULT),) + Projeto.ORIGEM_CHOICES
    )
