from model_mommy import mommy
from django.test import TestCase
from mock import patch


class UpdateCachedTagsSignalTestCase(TestCase):

    @patch('website.signals.UpdateCachedTags', spec=True)
    def test_post_save_calls_use_case_correctly(self, m_uc):
        mommy.make('main.Tag')
        assert m_uc().execute.called
