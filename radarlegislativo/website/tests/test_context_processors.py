# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2018 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.test import TestCase, override_settings

from website.context_processors import custom_fields, _get_value
from website.models import WebsiteOptions

__all__ = ["TestCustomValuesContextProcessor", "TestGetValue"]


class TestCustomValuesContextProcessor(TestCase):

    def test_default_for_custom_fields(self):
        result = custom_fields(None)
        self.assertEqual(u"Radar Legislativo", result['long_title'])

    def test_custom_fields_from_user(self):
        WebsiteOptions.objects.create(long_title="Foobar")
        result = custom_fields(None)
        self.assertEqual(u"Foobar", result['long_title'])

    @override_settings(TRAMITABOT_API_TOKEN="foobar")
    def test_custom_fields_with_bot(self):
        result = custom_fields(None)
        self.assertTrue(result['tramitabot'])

    @override_settings(TRAMITABOT_API_TOKEN=None)
    def test_custom_fields_without_bot(self):
        result = custom_fields(None)
        self.assertFalse(result['tramitabot'])


class TestGetValue(TestCase):

    def setUp(self):
        self.value = 42
        self.false = False
        self.none = None

    def test_false_value(self):
        self.assertFalse(_get_value(self, 'false'))

    def test_None_value(self):
        self.assertIsNone(_get_value(self, 'none'))

    def test_existing_value(self):
        self.assertEqual('42', _get_value(self, 'value'))
