# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import url
from website.views import (Principal, DetalheProjeto, Agenda,
                           NovosPLs, Tramitabot, CenarioLegislativo, CustomCSS,
                           Sobre, redirect_to_graphcommons)

urlpatterns = [
    url('^$', Principal.as_view(), name="principal"),
    url('^agenda/$', Agenda.as_view(), name="agenda"),
    url('^novos-pls/$', NovosPLs.as_view(), name="novos_pls"),
    url('^cenario-legislativo/$', CenarioLegislativo.as_view(),
        name="cenario_legislativo"),
    url('^grafo/$', redirect_to_graphcommons, name="grafo"),
    url('^sobre/$', Sobre.as_view(), name="sobre"),
    url('^tramitabot/$', Tramitabot.as_view(), name="tramitabot"),
    url('^projeto/(?P<pk>\d+)/$', DetalheProjeto.as_view(), name="projeto"),
    url('^custom.css$', CustomCSS.as_view(), name="custom-css"),
]
